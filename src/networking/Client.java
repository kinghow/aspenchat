package networking;

import chat.ChatView;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class Client {
    private final ExecutorService executor = Executors.newCachedThreadPool();
    private Socket socket;

    private final LinkedBlockingQueue<Object> packetsQueue = new LinkedBlockingQueue<>();

    private ObjectOutputStream out;
    private ObjectInputStream in;

    private User user;
    private ChatView chat;

    public Client(User user, ChatView chat) {
        this.user = user;
        this.chat = chat;
    }

    public void connect(String alias, String hostname, int port) throws IOException {
        socket = new Socket();

        socket.connect(new InetSocketAddress(hostname, port));

        // Ref: https://stackoverflow.com/questions/32939753/java-socket-stuck-at-new-objectinputstream
        // Always construct output stream before input stream to prevent deadlock.
        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());

        sendPacket(new User(alias, new InetSocketAddress(hostname, port)));

        executor.execute(this::readPackets);
        executor.execute(this::handlePackets);
    }

    public void close() throws IOException {
        socket.close();
        executor.shutdownNow();
        packetsQueue.clear();
    }

    public User getUser() {
        return user;
    }

    public void sendPacket(Object packet) {
        try {
            out.writeObject(packet);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void readPackets() {
        while (true) {
            try {
                Object packet = in.readObject();
                packetsQueue.put(packet);
            } catch (InterruptedException | IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void handlePackets() {
        while (true) {
            try {
                Object packet = packetsQueue.take();
                // Handle packets.
                if (packet instanceof Message) {
                    chat.appendToChatLog((Message) packet);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
