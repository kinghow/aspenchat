package networking;

import chat.ChatView;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class Server {
    private final ExecutorService executor = Executors.newCachedThreadPool();
    private ServerSocket socket;

    private final ArrayList<ClientConnection> clients = new ArrayList<>();
    // For easy synchronisation.
    private final LinkedBlockingQueue<Object> packetsQueue = new LinkedBlockingQueue<>();

    private ChatView chat;

    public Server(ChatView chat) {
        this.chat = chat;
    }

    public void start(String hostname, int port) throws IOException {
        socket = new ServerSocket();
        socket.bind(new InetSocketAddress(hostname, port));

        executor.execute(this::acceptConnections);
        executor.execute(this::handlePackets);
    }

    public void shutdown() throws IOException {
        socket.close();
        executor.shutdownNow();
        synchronized (clients) {
            clients.clear();
        }
        packetsQueue.clear();
    }

    public void echoAll(Object packet) {
        for (ClientConnection client : clients) {
            client.write(packet);
        }
    }

    public void removeClient(ClientConnection client) {
        clients.remove(client);
        try {
            client.shutdown();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Message connectedMessage = new Message(new User("SERVER", null), "A user has disconnected.");
        chat.appendToChatLog(connectedMessage);

        echoAll(connectedMessage);
    }

    private void acceptConnections() {
        while (!socket.isClosed()) {
            try {
                // Run every accepted client connection on a new thread.
                Socket clientSocket = socket.accept();
                ClientConnection connection = new ClientConnection(clientSocket, packetsQueue, this);
                synchronized (clients) {
                    clients.add(connection);
                }
                executor.execute(connection);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void handlePackets() {
        while (!socket.isClosed()) {
            try {
                Object packet = packetsQueue.take();
                // Handle packets.
                if (packet instanceof User) {
                    Message connectedMessage = new Message(new User("SERVER", null), ((User) packet).getAlias() + " has connected.");
                    chat.appendToChatLog(connectedMessage);

                    echoAll(connectedMessage);
                } else if (packet instanceof Message) {
                    chat.appendToChatLog((Message) packet);
                    echoAll(packet);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
