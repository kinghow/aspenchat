package networking;

import networking.User;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Message implements Serializable {
    private final ZonedDateTime timestamp;
    private final User source;
    private final String text;

    public Message(User source, String text) {
        timestamp = ZonedDateTime.now();
        this.source = source;
        this.text = text;
    }

    public String getTimestamp() {
        // (+08:00) 07-Mar-23 09:21PM
        return timestamp.format(DateTimeFormatter.ofPattern("(XXX) dd-MMM-yy hh:mma"));
    }

    public String getSourceAlias() {
        return source.getAlias();
    }

    public String getText() {
        return text;
    }
}
