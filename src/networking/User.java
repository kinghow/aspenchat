package networking;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;

public class User implements Serializable {
    private String alias;
    private final InetSocketAddress ip;

    public User(String alias, InetSocketAddress ip) {
        this.alias = alias;
        this.ip = ip;
    }

    public String getAlias() {
        return alias;
    }

    public InetSocketAddress getIp() {
        return ip;
    }

    @Override
    public String toString() {
        return "networking.User{" +
            "alias='" + alias + '\'' +
            ", ip=" + ip +
            '}';
    }
}
