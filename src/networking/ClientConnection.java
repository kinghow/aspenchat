package networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class ClientConnection implements Runnable {
    private final Socket socket;
    private final ExecutorService executor = Executors.newCachedThreadPool();
    // For easy synchronisation.
    private final LinkedBlockingQueue<Object> packetsQueue;

    private final ObjectOutputStream out;
    private final ObjectInputStream in;

    private final Server server;

    public ClientConnection(Socket socket, LinkedBlockingQueue<Object> packetsQueue, Server server) throws Exception {
        this.socket = socket;
        this.packetsQueue = packetsQueue;
        this.server = server;

        System.out.println("Making...");

        // Ref: https://stackoverflow.com/questions/32939753/java-socket-stuck-at-new-objectinputstream
        // Always construct output stream before input stream to prevent deadlock.
        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());
    }

    public void shutdown() throws IOException {
        socket.close();
        executor.shutdownNow();
    }

    public void write(Object packet) {
        try {
            out.writeObject(packet);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        executor.execute(this::listen);
    }

    private void listen() {
        while (!socket.isClosed()) {
            try {
                packetsQueue.put(in.readObject());
            } catch (InterruptedException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                server.removeClient(this);
            }
        }
    }
}
