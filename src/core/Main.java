package core;

import login.LoginController;
import login.LoginView;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("AspenChat");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            LoginView login = new LoginView();
            LoginController controller = new LoginController(login);

            frame.add(login);
            frame.pack();
            frame.setVisible(true);
        });
    }
}