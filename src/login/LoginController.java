package login;

import chat.ChatView;
import networking.Client;
import networking.Message;
import networking.Server;
import networking.User;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

public class LoginController {
    private final LoginView view;

    public LoginController(LoginView view) {
        this.view = view;

        view.setHostAction(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ChatView chat = new ChatView();
                    Server server = new Server(chat);
                    chat.setChatAction(new AbstractAction() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (!chat.getChatFieldText().isEmpty()) {
                                Message message = new Message(new User("SERVER", null), chat.getChatFieldText());

                                chat.appendToChatLog(message);
                                server.echoAll(message);
                                chat.clearChatField();
                            }
                        }
                    });

                    if (view.getIp().isEmpty()) {
                        server.start(InetAddress.getLocalHost().getHostAddress(), view.getPort());
                    } else {
                        server.start(view.getIp(), view.getPort());
                    }

                    JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(view);
                    frame.remove(view);

                    frame.add(chat);
                    frame.pack();
                    frame.revalidate();

                    Message startupMessage = new Message(new User("SERVER", null), "Successfully started.");
                    chat.appendToChatLog(startupMessage);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        view.setConnectAction(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ChatView chat = new ChatView();
                    User clientUser = new User(view.getAlias(), new InetSocketAddress(view.getIp(), view.getPort()));
                    Client client = new Client(clientUser, chat);
                    chat.setChatAction(new AbstractAction() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (!chat.getChatFieldText().isEmpty()) {
                                Message message = new Message(clientUser, chat.getChatFieldText());

                                client.sendPacket(message);
                                chat.clearChatField();
                            }
                        }
                    });

                    client.connect(view.getAlias(), view.getIp(), view.getPort());

                    JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(view);
                    frame.remove(view);

                    frame.add(chat);
                    frame.pack();
                    frame.revalidate();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
    }
}
