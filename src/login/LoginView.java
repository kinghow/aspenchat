package login;

import javax.swing.*;

public class LoginView extends JPanel {
    private final JTextField aliasField = new JTextField();
    private final JTextField ipField = new JTextField(11);
    private final JTextField portField = new JTextField(4);
    private final JLabel aliasLabel = new JLabel("Alias");
    private final JLabel ipLabel = new JLabel("Ip");
    private final JLabel portLabel = new JLabel("Port");

    private final JButton hostButton = new JButton("Host");
    private final JButton connectButton = new JButton("Connect");

    public LoginView() {
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addComponent(aliasLabel)
                .addComponent(ipLabel))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(aliasField)
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(ipField))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(portLabel))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(portField)))
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(hostButton))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(connectButton)))));

        layout.setVerticalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(aliasLabel).addComponent(aliasField))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(ipLabel).addComponent(ipField)
                .addComponent(portLabel).addComponent(portField))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(hostButton).addComponent(connectButton)));

        layout.linkSize(SwingConstants.HORIZONTAL, hostButton, connectButton);

        ipField.setText("localhost");
        portField.setText("6567");
    }

    public void setHostAction(Action action) {
        hostButton.setAction(action);
        hostButton.setText("Host"); // Set button text after setting action or button text won't show.
    }

    public void setConnectAction(Action action) {
        connectButton.setAction(action);
        connectButton.setText("Connect"); // Set button text after setting action or button text won't show.
    }

    public String getAlias() {
        return aliasField.getText();
    }

    public String getIp() {
        return ipField.getText();
    }

    public int getPort() {
        return Integer.parseInt(portField.getText());
    }
}
