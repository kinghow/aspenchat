package chat;

import networking.Message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class ChatMessages implements Serializable, Iterable {
    private ArrayList<Message> messages = new ArrayList<>();

    public void push(Message message) {
        messages.add(message);
    }

    @Override
    public Iterator iterator() {
        return messages.iterator();
    }

    @Override
    public void forEach(Consumer action) {
        Iterable.super.forEach(action);
    }

    @Override
    public Spliterator spliterator() {
        return Iterable.super.spliterator();
    }
}
