package chat;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class ChatCell extends JTextArea implements ListCellRenderer {
    public ChatCell() {
        this.setOpaque(true);
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.setLineWrap(true);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        this.setText("<html>" + value.toString() + "</html>");

        if (index % 2 == 0) {
            this.setAlignmentX(Component.RIGHT_ALIGNMENT);
            this.setAlignmentY(Component.RIGHT_ALIGNMENT);
        } else {
            this.setAlignmentX(Component.LEFT_ALIGNMENT);
            this.setAlignmentY(Component.LEFT_ALIGNMENT);
        }

        if (isSelected) {
            this.setBackground(list.getSelectionBackground());
            this.setForeground(list.getSelectionForeground());
        } else {
            this.setBackground(list.getBackground());
            this.setForeground(list.getForeground());
        }

        return this;
    }
}
