package chat;

import networking.Message;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class ChatView extends JPanel {
    private final JEditorPane chatLog = new JEditorPane();
    private final JTextField chatField = new JTextField();

    private final HTMLEditorKit kit = new HTMLEditorKit();
    private final HTMLDocument doc = new HTMLDocument();
    private String cumulativeChat = "";

    public ChatView() {
        chatLog.setEditorKit(kit);
        chatLog.setDocument(doc);
        chatLog.setEditable(false);
        //chatLog.setContentType("text/html");
        //chatLog.setLineWrap(true);
        //chatLog.setWrapStyleWord(true);

        JScrollPane scrollPane = new JScrollPane(chatLog);
        scrollPane.setPreferredSize(new Dimension(800, 600));

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup()
                .addComponent(scrollPane)
                .addComponent(chatField))
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
            .addComponent(scrollPane)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(chatField))
        );

        SwingUtilities.invokeLater(chatField::requestFocusInWindow);
    }

    public void setChatAction(Action action) {
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
            .put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), "chat");
        this.getActionMap().put("chat", action);
    }

    public String getChatFieldText() {
        return chatField.getText();
    }

    public void clearChatField() {
        chatField.setText("");
    }

    public void appendToChatLog(Message message) {
        SwingUtilities.invokeLater(() ->
            doChatLogText("<div><b>" + message.getTimestamp() + " [" + message.getSourceAlias() + "]</b> " + message.getText() + "</div>")
        );
    }

    private void doChatLogText(String text) {
        //cumulativeChat += text;
        //chatLog.setText(cumulativeChat);

        try {
            kit.insertHTML(doc, doc.getLength(), text, 0, 0, null);
        } catch (BadLocationException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //System.out.println(chatLog.getText());

//        HTMLDocument doc = (HTMLDocument) chatLog.getDocument();
//        Element[] roots = doc.getRootElements(); // #0 is the HTML element, #1 the bidi-root
//        Element body = null;
//        for (int i = 0; i < roots[0].getElementCount(); i++) {
//            Element element = roots[0].getElement(i);
//            if (element.getAttributes().getAttribute(StyleConstants.NameAttribute) == HTML.Tag.BODY) {
//                body = element;
//                break;
//            }
//        }
//
//        try {
//            doc.insertBeforeStart(body, text);
//        } catch (BadLocationException e) {
//            throw new RuntimeException(e);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }

//        HTMLDocument doc = (HTMLDocument) chatLog.getDocument();
//        try {
//            doc.insertAfterEnd(doc.getParagraphElement(doc.getLength()), text);
//            //doc.getDefaultRootElement()
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        } catch (BadLocationException e) {
//            throw new RuntimeException(e);
//        }

//        if (chatLog.getText().isEmpty()) {
//            chatLog.setText(text);
//        } else {
//            chatLog.setText(chatLog.getText() + "\n" + text);
//        }
    }
}
