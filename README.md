# AspenChat

## Summary

AspenChat is a simple chat program that allows users on different networks to chat with each other. Made with Java 11 and [IntelliJ IDEA](https://www.jetbrains.com/idea/download/?section=windows).

## Screenshot
![screenshot](screenshot.png)

## How to Run
1. Install IntellJ IDEA.
2. Build and run via IntellJ IDEA.
> **Note**: The host needs to port forward to accept clients.